Calendar for X11-Basic
======================

(c) Markus Hoffmann 2003-2021


Generates a calendar for any year, which can be printed to paper.

This program is written in X11-Basic and needs the packages  
transfig and psutil. 


Notitzkalender für X11-Basic
============================

(c) Markus Hoffmann 2003-2021


Generiert einen Notizkalender für jedes beliebige Jahr, 
welcher dann ausgedruckt werden kann.

* Wochenkalender mit viel Platz für Notizen,
* Monat, Kalenderwoche, Jahr, Tag und Wochentage,
* Mondphasen,
* Feiertage,
* geeignet fuer Schwarz-weiss- oder Farbdruck,
* Betriebsystemunabhängig.

Das Programm ist in X11-Basic geschrieben und benötigt die pakete 
transfig und psutil. 

Wenn Sie nur den Kalender wollen, können Sie für alle bisherigen Jahre und 
für das aktuelle Jahr einen Kalender als Datei (.pdf) zum Ausdrucken hier 
finden. Siehe die releases Sektion.

#### Download

Sources and binary packages:

<a href="https://codeberg.org/kollo/Notitzkalender/">
    <img alt="Get it on Codeberg" src="https://get-it-on.codeberg.org/get-it-on-blue-on-white.png" height="60">
</a>

(in section "releases")

### Important Note:

No software can be perfect. We do our best to keep this app bug free, 
improve it and fix all known errors as quick as possible. 
However, this program is distributed in the hope that it will 
be useful, but WITHOUT ANY WARRANTY; without even the implied 
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
Use this program on your own risk. 
Please report all errors, so we can fix them. 


    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; Version 2.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.



